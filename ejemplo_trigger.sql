﻿DROP DATABASE IF EXISTS ejemploTrigger;
CREATE DATABASE IF NOT EXISTS ejemploTrigger;
USE ejemploTrigger;

  CREATE OR REPLACE TABLE local (
    id int AUTO_INCREMENT,
    nombre varchar(50),
    precio float,
    plazas int,
    PRIMARY KEY (id)
  );

  CREATE OR REPLACE TABLE usuario (
    id int AUTO_INCREMENT,
    nombre varchar(50),
    inicial char(1),
    PRIMARY KEY (id)
  );

  CREATE OR REPLACE TABLE usa (
    local int,
    usario int,
    total float,
    fecha date,
    dias int,
    PRIMARY KEY (local,usario)
  );

  ALTER TABLE usa
    ADD CONSTRAINT fkUsaLocal FOREIGN KEY (local) REFERENCES local (id),
    ADD CONSTRAINT fkUsaUsuario FOREIGN KEY (usario) REFERENCES local (id);

-- 1. Quiero que cuando introduzca un usuario me calcule y almacene la inicia de su nombre.

  DELIMITER //
  CREATE OR REPLACE TRIGGER usuarioBI
    BEFORE INSERT
    ON usuario
    FOR EACH ROW
    BEGIN
      SET NEW.inicial = LEFT(NEW.nombre,1);
    END //
  DELIMITER ;

  INSERT INTO usuario (nombre)
    VALUES ('Roberto');

  SELECT * FROM usuario u;
