﻿DROP DATABASE IF EXISTS programacion;
CREATE DATABASE programacion;
USE programacion;

/*
Ejemplos de procedimientos almacenados
*/

/*
p1-crear un procedimiento almacenado 
    que muestre la fecha de hoy
*/

DELIMITER //
CREATE OR REPLACE PROCEDURE p1()
  BEGIN 
    SELECT NOW();
  END //
DELIMITER ;

CALL p1();

/*
  p2
  Crear una variable de tipo datetime 
  y que almacene en esa variable la fecha de hoy
*/

-- grabar el procedimiento

DELIMITER //
CREATE OR REPLACE PROCEDURE p2()
  BEGIN 
    -- declarar
    DECLARE v datetime;
    -- input (inicializar)
    SET v=NOW();
    -- output
    SELECT v;
  END //
DELIMITER ;

CALL p2();

/*
  p3
  Suma de dos números
  Reciba como argumentos dos numeros Y me muestre la suma de ellos
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE p3(num1 int,num2 int)
  BEGIN 
  DECLARE Suma int DEFAULT 0;
  SET suma=num1+num2;
  SELECT suma;
  END //
DELIMITER ;

CALL p3(1,5);
CALL p3(88,88);

/*
  p4
  Resta de dos números
  Reciba como argumentos dos numeros Y me muestre la suma de ellos
*/



DELIMITER //
CREATE OR REPLACE PROCEDURE p4(num1 int,num2 int)
  BEGIN 
  DECLARE resultado int DEFAULT 0;
  SET resultado=num1-num2;
  SELECT resultado;
  END //
DELIMITER ;

CALL p4(88,88);

/*
  p5
  Crear un procedimiento que almacene en una tabla lamada datos.
  la tabla la debe crear en caso de que no exista
*/
  DELIMITER //
  CREATE OR REPLACE PROCEDURE p5(n1 int, n2 int )
    BEGIN 
      -- declarar e inicializar las variables
      DECLARE resultado int DEFAULT 0;
      
      -- crear la tabla
      CREATE TABLE IF NOT EXISTS datos(
        id int AUTO_INCREMENT,
        d1 int DEFAULT 0,
        d2 int DEFAULT 0,
        suma int DEFAULT 0,
        PRIMARY KEY(id)
       );
    -- realizar los cálculos
    SET resultado=n1+n2;
    -- almacenar el resultado en la tabla
    INSERT INTO datos VALUES(DEFAULT,n1,n2,resultado);

  END //
  DELIMITER ;

CALL p5(14,5);
CALL p5(1,56);
SELECT * FROM datos;

/*
  p6
  procedimiento que calcula la SUMA y el producto de 2 NUMEROS
  Crear una tabla llamada sumaproducto(id,d1,d2,suma,producto).
  introducir los datos en la tabla y los resultados
*/

DELIMITER //
CREATE OR REPLACE PROCEDURE p6(n1 int, n2 int)
  BEGIN 
    -- crear variables
    DECLARE vsuma int DEFAULT 0;
    DECLARE vproducto int DEFAULT 0;
     -- realizar operaciones
    SET vSUMA=n1+n2;
    SET vPRODUCTO=n1*n2;
     
     -- crear la tabla
     CREATE TABLE IF NOT EXISTS sumaproducto(
          
          id int AUTO_INCREMENT,
          d1 int DEFAULT 0,
          d2 int DEFAULT 0,
          suma int DEFAULT 0,
          producto int DEFAULT 0,
          PRIMARY KEY(id)
         );
  
  INSERT INTO sumaproducto (id,d1,d2,suma,producto)
  VALUES (DEFAULT,n1,n2,vsuma,vproducto);


  END //
DELIMITER ;

CALL p6(4,5);
SELECT * FROM sumaproducto s;

/*
  p7
  Objetivo: elevar un número a otro. Almacenar
  el resultado y los números en la tabla potencia
  argumentos: base y el exponente call p7(2,3)
  el resultado será 8
*/

DELIMITER //
CREATE OR REPLACE PROCEDURE p7(base int,exponente int)
  BEGIN 
    DECLARE resultado int DEFAULT 0;


    SET resultado=POW(base,exponente);
    
    CREATE TABLE IF NOT EXISTS potencia(
      id int AUTO_INCREMENT,
      b int,
      e int,
      r int,
      PRIMARY KEY(id)
      );
      INSERT INTO potencia(b, e, r) -- campos
      VALUES (base,exponente,resultado); -- variables

  END //
DELIMITER ;

CALL p7(2,3);
SELECT * FROM potencia p;

/*
Objetivo: realizar la raiz cuadrada de un número
Almacenar el número y su raiz en la tabla raiz
*/

DELIMITER //
CREATE OR REPLACE PROCEDURE p8(n1 int)
  BEGIN 
    DECLARE resultado float DEFAULT 0;


    SET resultado= SQRT(n1);
    
    CREATE TABLE IF NOT EXISTS rcuadrada(
      id int AUTO_INCREMENT,
      n int,
      r float,
      PRIMARY KEY(id)
      );
      INSERT INTO rcuadrada(n, r) -- campos
      VALUES (n1,resultado); -- variables
  
  
  END //
DELIMITER ;

CALL p8(64);

SELECT * FROM rcuadrada r;

/*
Analizar el siguiente procedimiento y añadir 
lo necesario para que la longitud del texto
y lo alamcene en la tabla en un campo llamado
longitud.
*/
 -- modificacion
DELIMITER //
DROP PROCEDURE IF EXISTS p9 //

CREATE OR REPLACE PROCEDURE p9(argumento varchar(50))
  BEGIN 
      
    DECLARE l int DEFAULT 0;
    
    SET l= CHAR_LENGTH(argumento);
    
    CREATE TABLE IF NOT EXISTS texto(
      id int AUTO_INCREMENT PRIMARY KEY,
      texto varchar(50),
      longitud int DEFAULT 0
      );

    INSERT INTO texto(texto,longitud) 
    VALUE (argumento,l);
  
  
  END //
DELIMITER ;

CALL p9("helicoptero");

SELECT * FROM texto t;

/**
-- SELECT IF(numero>10,"grande","pequeño");
  IF (numero>10) THEN
    SELECT "grande";
  ELSE
    SELECT "pequeño";
  END IF
**/
/**
CONTROL DE FLUJO, INSTRUCCIONES DE SELECCION
**/
 
 /**
  SINTAXIS BASICA DEL IF
**/

/**  
  IF(condicion) THEN
    SENTENCIAS VERDADERO
  ELSE
    SENTENCIAS FALSO;
  END IF;
**/

/**
  P10
    Procedimiento al cual le paso un numero y me indica
    si es mayor que 100
**/

DELIMITER //
  CREATE OR REPLACE PROCEDURE p10(numero int)
    BEGIN 
      IF (numero>100) THEN
       SELECT "es mayor que 100";
      END IF;  
    END //
  DELIMITER ;
  
  CALL p10(120);
  CALL p10(90);

/**
  P11
    Procedimiento al cual le pasas un número y me 
    indica si es mayor igual o menor que 100
**/

-- Opción 1

DELIMITER //
  CREATE OR REPLACE PROCEDURE p11(numero int)
    BEGIN 
    DECLARE resultado varchar(50) DEFAULT "igual a 100";
      
      IF (numero>100) THEN
          SELECT "es mayor que 100";
      ELSEIF (numero<100) THEN
          SELECT "es menor que 100";
      ELSE       
          SELECT "es igual a 100";
      END IF;  
      SELECT resultado;
    
    END //
  DELIMITER ;

CALL p11(100);

-- Opción 2 (optimizada)

DELIMITER //
  CREATE OR REPLACE PROCEDURE p11(numero int)
    BEGIN 
    DECLARE resultado varchar(50) DEFAULT "igual a 100";
      
      IF (numero>100) THEN
          SET resultado="mayor que 100";
      ELSEIF (numero<100) THEN
          SET resultado= "menor que 100";
             
      END IF;  
      SELECT resultado;
    END //
  DELIMITER ;


CALL p11(100);

/**
  p12
  Crear un procedimiento que le pasas una nota y te debe devolver lo siguiente
  Suspenso: si la nota es menor que 5
  Suficiente: si la nota está entre 5 y 7
  Notable: si la nota está entre 7 y 9
  Sobresaliente: si la nota es mayor que 7
**/
/*
DELIMITER //
  CREATE OR REPLACE PROCEDURE p12(nota float)
    BEGIN 
    DECLARE resultado varchar(50) DEFAULT "Suspenso";
      
      IF (nota>7) THEN
          SET resultado="Sobresaliente";
      
      ELSE 
        IF (nota>= 7 AND nota<9) THEN
          SET resultado= "Notable";
        END
              
        IF (nota>= 5 AND nota<7) Then
          SET resultado ="Suficiente"
        END
        
              
      END IF; 
      SELECT resultado;
    END //
  DELIMITER ;


CALL p12(5);
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE p12(nota float)
    BEGIN 
    DECLARE resultado varchar(50) DEFAULT "Suspenso";
      
      IF (nota>=5 AND nota<7) THEN
          SET resultado="Suficiente";
      
      ELSEIF (nota>= 7 AND nota<9) THEN
          SET resultado= "Notable";
        
              
      ELSEIF (nota>9) Then
          SET resultado ="Sobresaliente";
      
      END IF; 
      SELECT resultado;
    END //
  DELIMITER ;

  CALL p12(5);

 -- El mismo ejercicio anterior pero modificado para CASE

DELIMITER //
  CREATE OR REPLACE PROCEDURE p13(nota float)
    BEGIN 
    DECLARE resultado varchar(50) ;
    CASE 
             
        WHEN (nota>=5 AND nota<7) THEN
            SET resultado="Suficiente";
        
        WHEN (nota>= 7 AND nota<9) THEN
            SET resultado= "Notable";
          
                
        WHEN (nota>9) Then
            SET resultado ="Sobresaliente";
      
        ELSE
            SET resultado="Suspenso";

     END CASE; 
      SELECT resultado;
    END //
  DELIMITER ;

  CALL p13(7);

  /**
p14
Quiero calcular la suma de dos números
La suma la almacena en un argumento de salida llamada resultado.
call p14(n1 int, n2 int, out resultado int);
**/

DELIMITER //
CREATE OR REPLACE PROCEDURE p14(n1 int, n2 int, OUT resultado int)
  BEGIN
  
    SET resultado=n1+n2; 
  
  END //
DELIMITER ;

SET @suma=0;
CALL p14(2,5,@suma);
SELECT @suma;

/**
p15
Procedimiento que recibe como argumentos:
  n1: número (argumento de entrada)
  n2: número (argumento de entrada)
  resultado: número (argumento de salida entrada/salida)
  cálculo: texto (argumento de entrada)
  Si calculo vale suma entonces resultado tedrá la suma de n1+n2
  Si calculo vale producto entonces resultado tendrá el producto de n1*n2
**/

DELIMITER //
CREATE OR REPLACE PROCEDURE p15(n1 int, n2 int, INOUT resultado int, calculo varchar(50))
  BEGIN
  
    CASE 
             
        WHEN (calculo="suma") THEN
            SET resultado= n1+n2;
        
        WHEN (calculo="producto") THEN
            SET resultado= n1*n2;
     END CASE;
  
  END //
DELIMITER ;


SET @suma=0;

CALL p15(2,5,@suma,"suma");

SELECT @suma;

SET @producto=0;

CALL p15(2,5,@producto,"producto");

SELECT @producto;

-- Opcion 2

DELIMITER //
CREATE OR REPLACE PROCEDURE p15(n1 int, n2 int, INOUT resultado int, calculo varchar(50))
  BEGIN
  
    IF (operacion="suma") THEN
      SET resultado=n1+n2;
    ELSEIF (operacion="producto") THEN
      SET resultado=n1*n2;
    END IF;
  
  END //
DELIMITER ;

CALL p15(1,5,@r,"suma");
SELECT @r;

/**
p16
Utilizando while
Con este procedimiento vamos a mostrar en pantalla números desde el 1 al 10.
**/
DELIMITER //
CREATE OR REPLACE PROCEDURE p16()
  BEGIN 
     -- Creamos una variable
     DECLARE contador int DEFAULT 1;
     
     -- Creamos un bucle
     WHILE (contador<=10) DO 
     -- Muestra la variable 
      SELECT contador;
     -- Incrementamos la variable en 1
      
      SET contador = contador + 1; 
     
     END WHILE;  
   
  END //
DELIMITER ;
CALL p16;

/**
p17
Utilizando while
Igual que el ej anterior pero meter los datos en una tabla temporal
**/

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p17()
    BEGIN 
      DECLARE contador int DEFAULT 1;
      -- esto es como una variable (ya que es temporal)
      CREATE OR REPLACE TEMPORARY TABLE numeros(
        numero  int
      );

      WHILE (contador<=10) DO 
        INSERT INTO numeros VALUES (contador);
        SET contador=contador+1; 
      END WHILE;
      
      SELECT * FROM numeros n;
    
    END //
  DELIMITER ;
  
  CALL p17;

/**
p18
Crear un procedimiento que le pasas un número y te muestra desde el número 1 hasta el número pasado.
Utilizar una TABLA TEMPORAL. Realizarlo con WHILE.
CALL P18(5);==>1,2,3,4,5
**/

DELIMITER //
  CREATE OR REPLACE PROCEDURE p18(numero int)
    BEGIN 
      DECLARE contador int DEFAULT 1;
      -- esto es como una variable (ya que es temporal)
      CREATE OR REPLACE TEMPORARY TABLE numeros(
        dato  int
      );

      WHILE (contador<=numero) DO 
        
        INSERT INTO numeros VALUE (contador);
        SET contador=contador+1; 
      END WHILE;
      
      SELECT * FROM numeros n;
    
    END //
DELIMITER ;
  
  CALL p18(20);

-- Con ITERATE

DELIMITER //
  CREATE OR REPLACE PROCEDURE p18a(numero int)
    BEGIN 
      DECLARE contador int DEFAULT 1;
      -- esto es como una variable (ya que es temporal)
      CREATE OR REPLACE TEMPORARY TABLE numeros(
        dato  int
      );

      label: WHILE (contador<=numero) DO 
        
        IF (contador=4) THEN
        SET contador=contador+1; 
          
          ITERATE label;
        END IF;

        INSERT INTO numeros VALUE (contador);
        SET contador=contador+1;
      
      END WHILE label;
      
      SELECT * FROM numeros n;
    
    END //
DELIMITER ;
  
CALL p18a(20);

  /**Con Funciones

  F1 
   Le paso dos números y me devuelve la suma de ellos.
 **/  
  DELIMITER //
  CREATE OR REPLACE FUNCTION f1(n1 int, n2 int)
    RETURNS INT
  BEGIN
    DECLARE resultado int DEFAULT 0;
    SET resultado=n1+n2;
    
    RETURN resultado;
  END //
  DELIMITER ;

 SELECT f1(5,6);